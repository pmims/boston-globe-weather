define(['baseModel'],
    function (BaseModel) {
        return forecastModel = BaseModel.extend({
            defaults: {
                zip: "94606"
            },
            url: function (zip) {
                return "https://api.openweathermap.org/data/2.5/forecast?zip=" + this.get('zip') + ",us&appid=a252f68200f6210ceaa0a8dc328c02b7&units=imperial"
            }
        });
    }); 
