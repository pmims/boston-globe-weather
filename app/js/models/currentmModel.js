define(['baseModel'],
    function (BaseModel) {
        return hourlyModel = BaseModel.extend({
            defaults: {
                city: "oakland",
                zip: "94606"
            },
            url: function (zip) {
                return "https://api.openweathermap.org/data/2.5/weather?q=" + this.get('city') + ",us&appid=a252f68200f6210ceaa0a8dc328c02b7&units=imperial"
            }
        });
    }); 
