define(['baseView', 'alertView', 'forecastView', 'hourlyView'], 
    function(baseView, alertView, forecastView, hourlyView) {
        return collectionViewBase = baseView.extend({
            initialize: function() {
                console.log("baseView");
                console.log("collectionViewBase");
                this.fetch();
                this.render();
            }
        });
    });
