define(['baseView'], 
    function(BaseView) {
        return hourlyView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },
            template: _.template($("#hourly-template").html())
        }); 
    });
