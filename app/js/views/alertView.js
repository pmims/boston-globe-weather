define(['baseView'], 
    function(BaseView) {
        return alertView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },

            template: _.template($("#alert-template").html())
        }); 
    });
