define(['baseView'], 
    function(BaseView) {
        return forecastView = BaseView.extend({
            initialize: function() {
                this.fetch();
            },

            template: _.template($("#forecast-template").html())
        }); 
    });
