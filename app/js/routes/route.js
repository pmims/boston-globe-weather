define(['baseRoute', 'hourlyModel'], 
    function(BaseRoute, hourlyModel) {
        return routes = BaseRoute.extend({
            routes: {
                'init'                  : '_init', 
                'changeUnit/:unit'      : '_changeUnit',
                'chart/:zip'            : '_chart',
                'daily/:zip'            : '_daily',
                'hourly/:zip'           : '_hourly',
                'main/:zip'             : '_main',
                'map'                   : '_map',
                'search'                : '_search',
                ''                      : '_start',
                'uv'                    : '_uv',
                '*default'              : '_default'
            }, 
            _init: function() {
                console.log("Init Route");
            },
            _changeUnit: function(unit) {
                new hourlyView({
                    el: "#hourly",
                    model: new hourlyModel({ unit: unit })
                });
            },
            _default: function() {
                console.log("Default Route");
            }
        });
    });
