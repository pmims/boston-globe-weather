define(['baseCollection'], 
    function(BaseCollection) {
        return collection = BaseCollection.extend({
            model: forecastModel,
            url: "https://api.openweathermap.org/data/2.5/weather?zip=94606,us&appid=a252f68200f6210ceaa0a8dc328c02b7&units=imperial"
        });
    });
