define(['baseCollection'], 
    function(BaseCollection) {
        return collection = BaseCollection.extend({
            defaults: {
                unit: "imperial"
            },
            model: forecastModel,
            url: function(unit) {
                return "https://api.openweathermap.org/data/2.5/weather?zip=94606,us&appid=a252f68200f6210ceaa0a8dc328c02b7&units=" + this.get("unit")
            }
        });
    });
