require.config ({
    baseUrl: 'app',
    paths: {
        /* lib */
        backbone:           '../../node_modules/backbone/backbone',
        jquery:             '../../node_modules/jquery/dist/jquery',
        luxon:              '../../node_modules/luxom/src/luxon', 
        momentTimeZone:     '../../node_modules/moment-timezone/moment-timezone', 
        moment:             '../../node_modules/moment/moment', 
        underscore:         '../../node_modules/underscore/underscore',
        vue:                '../../node_modules/vue/dist/vue',

        /* m */
        baseModel:          'js/models/baseModel',
        alertModel:         'js/models/alertModel',
        currentModel:       'js/models/currentModel',
        forecastModel:      'js/models/forecastModel',
        hourlyModel:        'js/models/hourlyModel',
        hourlyCelciusModel: 'js/models/hourlyCelciusModel',

        /* v */
        baseView:           'js/views/baseView',
        baseCollectionView: 'js/views/baseCollectionView',
        alertView:          'js/views/alertView',
        currentView:        'js/views/currentView',
        collectionView:     'js/views/collectionView',
        forecastView:       'js/views/forecastView',
        hourlyView:         'js/views/hourlyView',

        /* c */
        baseCollection:     'js/collections/baseCollection',
        collection:         'js/collections/collection',

        /* r */
        baseRoute:          'js/routes/baseRoute',
        route:              'js/routes/route'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    }
});

define(['collectionView', 'collection', 'route'], 
    function(collectionView, collection, route) {
        new route;
        Backbone.history.start();

        new collectionView ({
            collection: new collection
        });
    }); 
